import FavoriteRestaurantIdb from "../src/scripts/data/favoriterestaurant-idb";
import FavoriteRestaurantSearchPresenter2 from "../src/scripts/views/pages/liked-restaurant/favorite-restaurant-search-presenter-2";

describe('Searching restaurants', () => {
    let presenter;
    let favoriteRestaurants2;

    const searchRestaurant2 = (query) => {
        const queryElement = document.getElementById('query');
        queryElement.value = query;
        queryElement.dispatchEvent(new Event('change'));
    }

    const setRestaurantSearchContainer = () => {
        document.body.innerHTML = `
          <div id="restaurant-search-container">
              <input id="query" type="text">
              <div class="restaurant-result-container">
                  <ul class="restaurants">
                  </ul>
              </div>
          </div>
          `;
    };

    const constructPresenter = () => {
        favoriteRestaurants2 = spyOnAllFunctions(FavoriteRestaurantIdb);
        presenter = new FavoriteRestaurantSearchPresenter2({
            favoriteRestaurants2,
        });
    };

    beforeEach(() => {
        setRestaurantSearchContainer();
        constructPresenter();
    });

    describe('When query is not empty', () => {


        it('should be able to capture the query typed by the user', () => {

            searchRestaurant2('restaurant a');

            expect(presenter.latestQuery).toEqual('restaurant a');
        });

        it('should ask the model to search for liked restaurant', () => {

            searchRestaurant2('restaurant a');

            expect(favoriteRestaurants2.searchRestaurant2)
                .toHaveBeenCalledOnceWith('restaurant a');
        });

        it('should show the found restaurants', () => {

            presenter._showFoundRestaurants([{ id: 1 }]);

            expect(document.querySelectorAll('.restaurant').length).toEqual(1);


            presenter._showFoundRestaurants([{ id: 1, title: 'Test1' }, { id: 2, title: 'Test2' }]);

            expect(document.querySelectorAll('.restaurant').length).toEqual(2);
        });

        it('should show the title of the found restaurants', () => {
            presenter._showFoundRestaurants([{ id: 1, title: 'Test1' }]);
            expect(document.querySelectorAll('.restaurant__title').item(0).textContent)
                .toEqual('Test1');

            presenter._showFoundRestaurants(
                [{ id: 1, title: 'Test1' }, { id: 2, title: 'Test2' }],
            );

            const restaurantTitles = document.querySelectorAll('.restaurant__title');
            expect(restaurantTitles.item(0).textContent).toEqual('Test1');
            expect(restaurantTitles.item(1).textContent).toEqual('Test2');
        });

        it('should show - for found restaurant without title', () => {
            presenter._showFoundRestaurants([{ id: 1 }]);

            expect(document.querySelectorAll('.restaurant__title').item(0).textContent)
                .toEqual('-');
        });


        it('should show the restaurants found by Favorite Restaurants', (done) => {
            document.getElementById('restaurant-search-container')
                .addEventListener('restaurants:searched:updated', () => {
                    expect(document.querySelectorAll('.restaurant').length).toEqual(3);
                    done();
                });

            favoriteRestaurants2.searchRestaurant2.withArgs('restaurant a').and.returnValues([
                { id: 1, title: 'restaurant abc' },
                { id: 2, title: 'iec restaurant agata' },
                { id: 3, title: 'park restaurant amber' },
            ]);

            searchRestaurant2('restaurant a');

        });

        it('should show the name of the restaurants found by Favorite Restaurants', (done) => {
            document.getElementById('restaurant-search-container').addEventListener('restaurants:searched:updated', () => {
                const restaurantTitles = document.querySelectorAll('.restaurant__title');
                expect(restaurantTitles.item(0).textContent).toEqual('restaurant abc');
                expect(restaurantTitles.item(1).textContent).toEqual('restaurant luxury agata');
                expect(restaurantTitles.item(2).textContent).toEqual('restaurant kitchen amber');
                done();
            });

            favoriteRestaurants2.searchRestaurant2.withArgs('restaurant a').and.returnValues([
                { id: 1, title: 'restaurant abc' },
                { id: 2, title: 'restaurant luxury agata' },
                { id: 3, title: 'restaurant kitchen amber' },
            ]);

            searchRestaurant2('restaurant a');
        });


    });


    describe('When query is empty', () => {
        it('should capture the query as empty', () => {
            searchRestaurant2(' ');
            expect(presenter.latestQuery.length).toEqual(0);

            searchRestaurant2('    ');
            expect(favoriteRestaurants2.getAllRestaurants)
                .toHaveBeenCalled();
            expect(presenter.latestQuery.length).toEqual(0);

            searchRestaurant2('');
            expect(presenter.latestQuery.length).toEqual(0);

            searchRestaurant2('\t');
            expect(presenter.latestQuery.length).toEqual(0);
        })
    });

    describe('when no favorite restaurants could be found', () => {
        it('should show the empty message', (done) => {
            document.getElementById('restaurant-search-container')
                .addEventListener('restaurants:searched:updated', () => {
                    expect(document.querySelectorAll('.restaurants__not__found').length)
                        .toEqual(1);
                    done();
                });
            favoriteRestaurants2.searchRestaurant2.withArgs('restaurant a').and.returnValues([]);

            searchRestaurant2('restaurant a')
        });

        it('should not show any restaurants', () => {
            document.getElementById('restaurant-search-container').addEventListener('restaurants:searched-updated', () => {
                expect(document.querySelectorAll('.restaurant').length).toEqual(0);
                done();
            });

            favoriteRestaurants2.searchRestaurant2.withArgs('restaurant a').and.returnValues([]);

            searchRestaurant2('restaurant a');
        })
    })











});
