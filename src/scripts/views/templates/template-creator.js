import CONFIG from '../../globals/config';

const createLoaderTemplate = {
  show() {
    return `
        <div class="page__loader"></div>
      `;
  },
  remove() {
    document.querySelector('.page__loader').remove();
  },
};

const createRestaurantDetailTemplate = (restaurant) => `
<div class="container__detail">
        <img class="detail__poster" src="${
          CONFIG.BASE_IMAGE_URL('large') + restaurant.pictureId
        }" alt="${restaurant.name}" />

        <div class="centered">
            <h1 class="detail__title">${restaurant.name}</h1>
        </div>

        <div class="detail__bottom-left">
            <h2>⭐️ ${restaurant.rating}</h2>
        </div>
    </div>
    <div class="detail__info">
        <h2 class="detail__title__description">Location</h2>
        <div class="detail__area">
            <p>${restaurant.city}</p>
            <p>${restaurant.address}</p>
        </div>

        <h2 class="detail__title__description">Categories</h2>
        <div class="detail__area">
            <ul class="detail__categories">
                ${restaurant.categories
                  .map(
                    (category) => `
                <li class="detail__categories__style">${category.name}</li>
                `
                  )
                  .join('')}
            </ul>
        </div>

        <h2 class="detail__title__description">Food</h2>
        <div class="detail__area">
            <ul class="detail__list__data">
                ${restaurant.menus.foods
                  .map(
                    (food) => `
                <li class="detail__categories__style">${food.name}</li>
                `
                  )
                  .join('')}
            </ul>
        </div>

        <h2 class="detail__title__description">Drink</h2>
        <div class="detail__area">
            <ul class="detail__list__data">
                ${restaurant.menus.drinks
                  .map(
                    (drink) => `
                <li class="detail__categories__style">${drink.name}</li>
                `
                  )
                  .join('')}
            </ul>
        </div>


        <h3 class="detail__title__description">Description</h3>
        <p class="detail__overview">${restaurant.description}</p>

        <h2 class="detail__title__description">Costumer Reviews</h2>
        <div class="detail__overview">
            <ul class="detail__panel__area">
                ${restaurant.customerReviews
                  .map(
                    (review) => `
                <li class="detail__user1">${review.name}</li>
                <li class="detail__user2">Date: ${review.date}</li>
                <li class="detail__user2">Review: ${review.review}</li>
                `
                  )
                  .join('')}
            </ul>
        </div>
    </div>
`;

const createRestaurantItemTemplate = (restaurant) => `
    <article class="restaurant-item">
        <img tabindex="0" class="restaurant-item__thumbnail" src="${
          CONFIG.BASE_IMAGE_URL('small') + restaurant.pictureId
        }"
            alt="${restaurant.name}" />
        <div class="restaurant-item__content">
            <div class="restaurant-item__rating">
                <p tabindex="0" class="restaurant-item__loc">${
                  restaurant.city
                }</p>
                <p tabindex="0" class="checked">Rating ${restaurant.rating}</p>
            </div>
            <h1 class="restaurant-item__title">
                <a href="${`/#/detail/${restaurant.id}`}">${restaurant.name}</a>
            </h1>
            <p tabindex="0" class="restaurant-item__description">
                ${restaurant.description}
            </p>
        </div>
    </article>
`;

const createRestaurantFavoriteTemplate = (restaurant) => `
    <div class="favorite__content">
        <div class="favorite__image">
            <img class="favorite__image__item" src="${
              CONFIG.BASE_IMAGE_URL('small') + restaurant.pictureId
            }"
                alt="${restaurant.name}" />
        </div>
        <h2 class="favorite1"><a href="${`/#/detail/${restaurant.id}`}">${
  restaurant.name
}</a></h2>
        <p tabindex="0" class="favorite__image__description">
            ${restaurant.description}
        </p>
    </div>
`;

const createLikeRestaurantButtonTemplate = () => `
    <button aria-label="like this restaurant" id="likeButton" class="like">
        <i class="fa fa-heart-o" aria-hidden="true"></i>
    </button>
`;

const createUnlikeRestaurantButtonTemplate = () => `
    <button aria-label="unlike this restaurant" id="likeButton" class="like">
        <i class="fa fa-heart" aria-hidden="true"></i>
    </button>
`;

// eslint-disable-next-line import/prefer-default-export
export {
  createLoaderTemplate,
  createRestaurantDetailTemplate,
  createRestaurantItemTemplate,
  createLikeRestaurantButtonTemplate,
  createUnlikeRestaurantButtonTemplate,
  createRestaurantFavoriteTemplate,
};
