class FavoriteRestaurantSearchPresenter2 {
    constructor({ favoriteRestaurants2 }) {
        this._listenToSearchRequestByUser();
        this._favoriteRestaurants2 = favoriteRestaurants2;
    }

    _listenToSearchRequestByUser() {
        this._queryElement = document.getElementById('query');
        this._queryElement.addEventListener('change', (event) => {
            this._searchRestaurant2(event.target.value);
        });
    }

    _searchRestaurant2(latestQuery) {
        this._latestQuery = latestQuery;
        this._favoriteRestaurants2.searchRestaurant2(this.latestQuery);
    }


    get latestQuery() {
        return this._latestQuery;
    }

    _showFoundRestaurants(restaurants) {

        let html;
        if (restaurants.length > 0) {
            html = restaurants.reduce(
                (carry, restaurant) => carry.concat(`<li class="restaurant"><span class="restaurant__title">${restaurant.title || '-'}</span></li>`),
                ''
            );
        } else {
            html = '<div class="restaurants__not__found">Restaurant tidak ditemukan</div>';
        }

        document.querySelector('.restaurants').innerHTML = html;

        document.getElementById('restaurant-search-container')
            .dispatchEvent(new Event('restaurants:searched:updated'));
    }


    async _searchRestaurant2(latestQuery) {
        this._latestQuery = latestQuery.trim();

        let foundRestaurants;
        if (this.latestQuery.length > 0) {
            foundRestaurants = await this._favoriteRestaurants2.searchRestaurant2(this.latestQuery);
        } else {
            foundRestaurants = await this._favoriteRestaurants2.getAllRestaurants();
        }

        this._showFoundRestaurants(foundRestaurants)
    }
}

export default FavoriteRestaurantSearchPresenter2;
